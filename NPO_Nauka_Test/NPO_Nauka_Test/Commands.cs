﻿using System.Collections.Generic;

namespace NPO_Nauka_Test
{
    public static class Commands
    {
        public const string Close = "0";
        public const string Open = "1";
        public const string On = "on";
        public const string Off = "off";
        public const string Diagn = "diagn";
        public const string Vent = "vent";
        public const string Measure = "measure";
        public static List<string> GTCommands = new List<string>() { On, Off, Diagn, Vent, Measure };
    }
}
