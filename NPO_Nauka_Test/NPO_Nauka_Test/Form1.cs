﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static NPO_Nauka_Test.Commands;
using static NPO_Nauka_Test.Names;

namespace NPO_Nauka_Test
{
    public partial class Form1 : Form
    {
        private PLC plc;
        //событие окончания процесса
        public delegate void Process();
        public event Process Stop;

        public Form1()
        {
            InitializeComponent();
            plc = new PLC();
            plc.ValveStateChanged += DisplayValveState;
            plc.GTStateChanged += DisplayGTState;
            plc.ValueChanged += ValueChanged;
            Stop += OnStop;
        }
        
        private void Run()
        {
            Task task = new Task(() =>
            {
                try
                {
                    plc.SendPLC(VE1, Open);
                    Thread.Sleep(100);
                    plc.SendPLC(VE2, Open);
                    Thread.Sleep(100);
                    plc.SendPLC(VE3, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(VE5, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(NP, Open);
                    Thread.Sleep(100);
                    if (plc.GetPLC(PE2) > 20)
                        plc.PumpOutCamera();
                    plc.SendPLC(VE2, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(NP, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(VE3, Open);
                    Thread.Sleep(100);
                    if (plc.GetPLC(PE1) < 99)
                        plc.PumpIn(PE1);
                    plc.SendPLC(GT, On);
                    if (plc.GetPLC(GT) == -1)
                    {
                        object[] args = new object[2];
                        args[0] = InfoTextBox;
                        args[1] = "ГТ не готов.";
                        InfoTextBox.BeginInvoke(new InvokeTextValue(InvokeValueChanged), args);
                        return;
                    }
                    plc.SendPLC(GT, Measure);
                    if (plc.GetPLC(GT) >= 50.7)
                    {
                        object[] args = new object[2];
                        args[0] = InfoTextBox;
                        args[1] = "Показание потока гелия за пределами допуска.";
                        InfoTextBox.BeginInvoke(new InvokeTextValue(InvokeValueChanged), args);
                        return;
                    }
                    plc.SendPLC(VE6, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(VE10, Open);
                    Thread.Sleep(100);
                    if (plc.GetPLC(BP) >= 15)
                    {
                        plc.PumpOutBalloon();
                    }
                    plc.SendPLC(VE8, Open);
                    Thread.Sleep(100);
                    plc.SendPLC(RD, Open);
                    Thread.Sleep(100);
                    plc.SupplyHelium();
                    {
                        double helium = plc.GetPLC(GT);
                        object[] args = new object[2];
                        args[0] = InfoTextBox;
                        args[1] = "Показание потока гелия: " + helium;
                        InfoTextBox.BeginInvoke(new InvokeTextValue(InvokeValueChanged), args);
                    }
                    plc.SendPLC(VE8, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(VE6, Open);
                    Thread.Sleep(100);
                    plc.SendPLC(RD, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(GT, On);
                    Thread.Sleep(100);
                    plc.SendPLC(VE1, Commands.Close);
                    Thread.Sleep(100);
                    plc.SendPLC(VE5, Open);
                    Thread.Sleep(100);
                    plc.PumpIn(PE2);

                    Stop();
                }
                catch (Exception e)
                {
                    object[] args = new object[2];
                    args[0] = InfoTextBox;
                    args[1] = e.Message;
                    InfoTextBox.BeginInvoke(new InvokeTextValue(InvokeValueChanged), args);
                    return;
                }
            });
            task.Start();
        }

        public void DisplayValveState(string name, bool state)
        {
            foreach (Control control in groupBoxValves.Controls)
            {
                if (control is Button)
                {
                    if (control.Name.Contains(name))
                    {
                        if (state)
                            control.BackColor = Color.Lime;
                        else control.BackColor = Color.Red;
                    }
                }
            }
        }

        public void DisplayGTState(string state)
        {
            switch (state)
            {
                case On:
                    GTState.BackColor = Color.Lime;
                    break;
                case Off:
                    GTState.BackColor = Color.FromKnownColor(KnownColor.InactiveCaption);
                    break;
                case Diagn:
                    GTState.BackColor = Color.Red;
                    break;
                case Vent:
                    GTState.BackColor = Color.DodgerBlue;
                    break;
                case Measure:
                    GTState.BackColor = Color.Yellow;
                    break;
            }
        }

        public delegate void InvokeTextValue(TextBox textBox, string value);
        public delegate void InvokeButtonRun();

        public void InvokeValueChanged(TextBox textBox, string value)
        {
            textBox.Text = value.ToString();
        }

        public void ValueChanged(string name, string value)
        {
            foreach (Control control in groupBoxDetectors.Controls)
            {
                if (control is TextBox)
                {
                    if (control.Tag.ToString().Contains(name))
                    {
                        TextBox textBox = (TextBox)control;
                        object[] args = new object[2];
                        args[0] = textBox;
                        args[1] = value;
                        textBox.BeginInvoke(new InvokeTextValue(InvokeValueChanged), args);
                        return;
                    }
                }
            }
            if (name == "InfoTextBox")
            {
                object[] args = new object[2];
                args[0] = InfoTextBox;
                args[1] = value;
                InfoTextBox.BeginInvoke(new InvokeTextValue(InvokeValueChanged), args);
            }
        }

        private void BtnRun_Click(object sender, EventArgs e)
        {
            Run();
            BtnRun.Enabled = false;
            InfoTextBox.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            plc.Init();
        }

        public void OnStop()
        {
            BtnRun.BeginInvoke(new InvokeButtonRun(InvokeOnStop));
        }

        public void InvokeOnStop()
        {
            InfoTextBox.Text = "Процесс завершён.";
        }
    }
}
