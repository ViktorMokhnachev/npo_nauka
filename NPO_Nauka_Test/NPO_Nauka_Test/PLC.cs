﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using static NPO_Nauka_Test.Commands;
using static NPO_Nauka_Test.Names;

namespace NPO_Nauka_Test
{
    public class PLC
    {
        //значения измерительных приборов РЕ, ВР и ГТ
        private Dictionary<string, double> values;
        //состояния клапанов VE и РД и насоса NP
        private Dictionary<string, bool> states;
        //состояние ГТ
        private string gt;
        //Доступность баллона Б1, предположим, что он доступен с вероятностью 70%
        private bool b1;
        //Доступность баллона Б2, предположим, что он доступен с вероятностью 90%
        private bool b2;
        //событие при изменении состояния клапана или насоса
        public delegate void ValveState(string name, bool state);
        public event ValveState ValveStateChanged;
        //событие при изменении состояния ГТ
        public delegate void GTState(string state);
        public event GTState GTStateChanged;
        //событие при изменении значения измерительного прибора
        public delegate void Value(string name, string value);
        public event Value ValueChanged;

        public void Init()
        {
            //придумываем случайные показатели приборов в момент запуска
            Random random = new Random();
            values = new Dictionary<string, double>();
            values.Add(PE1, Math.Round(random.Next(100) + random.Next(10) / 10.0, 1));
            ValueChanged(PE1, values[PE1].ToString());
            values.Add(PE2, Math.Round(random.Next(100) + random.Next(10) / 10.0, 1));
            ValueChanged(PE2, values[PE2].ToString());
            states = new Dictionary<string, bool>();
            //VE1 изначально закрыт
            states.Add(VE1, false);
            ValveStateChanged(VE1, false);
            //VE2 изначально закрыт
            states.Add(VE2, false);
            ValveStateChanged(VE2, false);
            //VE3 изначально открыт
            states.Add(VE3, true);
            ValveStateChanged(VE3, true);
            //VE5 изначально открыт
            states.Add(VE5, true);
            ValveStateChanged(VE5, true);
            //VE6 изначально открыт
            states.Add(VE6, true);
            ValveStateChanged(VE6, true);
            //VE8 изначально закрыт
            states.Add(VE8, false);
            ValveStateChanged(VE8, false);
            //VE10 изначально закрыт
            states.Add(VE10, false);
            ValveStateChanged(VE10, false);
            //VE11 изначально закрыт
            states.Add(VE11, false);
            ValveStateChanged(VE11, false);
            //NP изначально отключён
            states.Add(NP, false);
            ValveStateChanged(NP, false);
            //РД изначально закрыт
            states.Add(RD, false);
            ValveStateChanged(RD, false);
        }

        public void SendPLC(string name, string command)
        {
            if (name == VE10 && command == Open)
            {
                Random random = new Random();
                double rnd = random.NextDouble();
                b1 = rnd <= 0.7;
                if (b1)
                {
                    states[VE10] = true;
                    ValveStateChanged(VE10, true);
                    values.Add(BP, Math.Round(random.Next(100) + random.Next(10) / 10.0, 1));
                }
                else
                {
                    rnd = random.NextDouble();
                    b2 = rnd <= 0.9;
                    if (b2)
                    {
                        states[VE11] = true;
                        ValveStateChanged(VE11, true);
                        values.Add(BP, Math.Round(random.Next(100) + random.Next(10) / 10.0, 1));
                    }
                    else
                    {
                        throw new Exception("Нет доступных баллонов");
                    }
                }
            }
            else if (valves.Contains(name))
            {
                switch (command)
                {
                    case Close:
                        states[name] = false;
                        ValveStateChanged(name, false);
                        break;
                    case Open:
                        states[name] = true;
                        ValveStateChanged(name, true);
                        break;
                    default:
                        throw new Exception("Неизестная команда.");
                }
            }
            else if (name == GT)
            {
                if (GTCommands.Contains(command))
                {
                    gt = command;
                    GTStateChanged(command);
                    if (command == On)
                    {
                        Random random = new Random();
                        double GTValue = Math.Round(random.Next(101) - random.Next(10) / 10.0, 1);
                        if (GTValue < 0 || GTValue > 99.9)
                            GTValue = -1;
                        if (!values.Keys.Contains(GT))
                            values.Add(GT, GTValue);
                        else values[GT] = GTValue;
                        Thread.Sleep(1000);
                    }
                    else if (command == Measure)
                    {
                        ValueChanged("InfoTextBox", "Идёт измерение потока гелия...");
                        Thread.Sleep((new Random()).Next(1000, 10000));
                        ValueChanged("InfoTextBox", "");
                        ValueChanged(GT, values[GT].ToString());
                    }
                }
            }
            else throw new Exception("Неверно указано устройство.");
        }

        public double GetPLC(string name)
        {
            if (!valves.Contains(name))
            {
                if (name != GT)
                    ValueChanged(name, values[name].ToString());
                return values[name];
            }
            else throw new Exception("Неверно указано устройство.");
        }

        public void PumpOutCamera()
        {
            Random random = new Random();
            while (values[PE2] > 20)
            {
                values[PE2] -= random.NextDouble();
                values[PE2] = Math.Round(values[PE2], 1);
                ValueChanged(PE2, values[PE2].ToString());
                Thread.Sleep(100);
            }
        }

        public void PumpOutBalloon()
        {
            Random random = new Random();
            while (values[BP] >= 15)
            {
                values[BP] -= random.NextDouble();
                values[BP] = Math.Round(values[BP], 1);
                ValueChanged(BP, values[BP].ToString());
                Thread.Sleep(100);
            }
        }

        public void PumpIn(string PE)
        {
            if (PE == PE1 || PE == PE2)
            {
                Random random = new Random();
                double vol;
                if (PE == PE1)
                    vol = 99;
                else vol = 99.9;
                while (values[PE] < vol)
                {
                    values[PE] += random.NextDouble() / 2.0;
                    values[PE] = Math.Round(values[PE], 1);
                    if (values[PE] >= 100)
                        values[PE] = 99.9;
                    ValueChanged(PE, values[PE].ToString());
                    Thread.Sleep(100);
                }
            }
            else throw new Exception("Неверное устройство.");
        }

        public void SupplyHelium()
        {
            Random random = new Random();
            ValueChanged("InfoTextBox", "Идёт подача гелия...");
            for (int i = 0; i < 300; i++)
            {
                bool increase = random.NextDouble() < 0.5;
                if (increase)
                    values[GT] += random.NextDouble() / 2.0;
                else values[GT] -= random.NextDouble() / 2.0;
                values[GT] = Math.Round(values[GT], 1);
                if (values[GT] < 0)
                    values[GT] = 0;
                if (values[GT] >= 100)
                    values[GT] = 99.9;
                ValueChanged(GT, values[GT].ToString());
                Thread.Sleep(100);
            }
        }
    }
}
