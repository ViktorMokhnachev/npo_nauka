﻿namespace NPO_Nauka_Test
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.PE1Value = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxDetectors = new System.Windows.Forms.GroupBox();
            this.PE2Value = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.GTState = new System.Windows.Forms.Button();
            this.BPValue = new System.Windows.Forms.TextBox();
            this.GTValue = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxValves = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.VE11State = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.VE10State = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.VE8State = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.VE6State = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.NPState = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.VE5State = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.VE3State = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.VE2State = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.VE01State = new System.Windows.Forms.Button();
            this.InfoTextBox = new System.Windows.Forms.TextBox();
            this.BtnRun = new System.Windows.Forms.Button();
            this.groupBoxDetectors.SuspendLayout();
            this.groupBoxValves.SuspendLayout();
            this.SuspendLayout();
            // 
            // PE1Value
            // 
            this.PE1Value.Location = new System.Drawing.Point(33, 19);
            this.PE1Value.Name = "PE1Value";
            this.PE1Value.ReadOnly = true;
            this.PE1Value.Size = new System.Drawing.Size(49, 20);
            this.PE1Value.TabIndex = 0;
            this.PE1Value.Tag = "РЕ1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "PE1";
            // 
            // groupBoxDetectors
            // 
            this.groupBoxDetectors.Controls.Add(this.PE2Value);
            this.groupBoxDetectors.Controls.Add(this.label13);
            this.groupBoxDetectors.Controls.Add(this.GTState);
            this.groupBoxDetectors.Controls.Add(this.BPValue);
            this.groupBoxDetectors.Controls.Add(this.GTValue);
            this.groupBoxDetectors.Controls.Add(this.label2);
            this.groupBoxDetectors.Controls.Add(this.label7);
            this.groupBoxDetectors.Controls.Add(this.PE1Value);
            this.groupBoxDetectors.Controls.Add(this.label1);
            this.groupBoxDetectors.Location = new System.Drawing.Point(12, 12);
            this.groupBoxDetectors.Name = "groupBoxDetectors";
            this.groupBoxDetectors.Size = new System.Drawing.Size(177, 106);
            this.groupBoxDetectors.TabIndex = 2;
            this.groupBoxDetectors.TabStop = false;
            this.groupBoxDetectors.Text = "Измерения";
            // 
            // PE2Value
            // 
            this.PE2Value.Location = new System.Drawing.Point(115, 20);
            this.PE2Value.Name = "PE2Value";
            this.PE2Value.ReadOnly = true;
            this.PE2Value.Size = new System.Drawing.Size(49, 20);
            this.PE2Value.TabIndex = 6;
            this.PE2Value.Tag = "РЕ2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(88, 23);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "PE2";
            // 
            // GTState
            // 
            this.GTState.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.GTState.Enabled = false;
            this.GTState.Location = new System.Drawing.Point(87, 71);
            this.GTState.Name = "GTState";
            this.GTState.Size = new System.Drawing.Size(20, 20);
            this.GTState.TabIndex = 5;
            this.GTState.UseVisualStyleBackColor = false;
            // 
            // BPValue
            // 
            this.BPValue.Location = new System.Drawing.Point(33, 45);
            this.BPValue.Name = "BPValue";
            this.BPValue.ReadOnly = true;
            this.BPValue.Size = new System.Drawing.Size(49, 20);
            this.BPValue.TabIndex = 2;
            this.BPValue.Tag = "ВР";
            // 
            // GTValue
            // 
            this.GTValue.Location = new System.Drawing.Point(33, 71);
            this.GTValue.Name = "GTValue";
            this.GTValue.ReadOnly = true;
            this.GTValue.Size = new System.Drawing.Size(49, 20);
            this.GTValue.TabIndex = 0;
            this.GTValue.Tag = "ГТ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "ВР";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "ГТ";
            // 
            // groupBoxValves
            // 
            this.groupBoxValves.Controls.Add(this.label11);
            this.groupBoxValves.Controls.Add(this.VE11State);
            this.groupBoxValves.Controls.Add(this.label12);
            this.groupBoxValves.Controls.Add(this.VE10State);
            this.groupBoxValves.Controls.Add(this.label10);
            this.groupBoxValves.Controls.Add(this.VE8State);
            this.groupBoxValves.Controls.Add(this.label6);
            this.groupBoxValves.Controls.Add(this.VE6State);
            this.groupBoxValves.Controls.Add(this.label9);
            this.groupBoxValves.Controls.Add(this.NPState);
            this.groupBoxValves.Controls.Add(this.label8);
            this.groupBoxValves.Controls.Add(this.VE5State);
            this.groupBoxValves.Controls.Add(this.label5);
            this.groupBoxValves.Controls.Add(this.VE3State);
            this.groupBoxValves.Controls.Add(this.label4);
            this.groupBoxValves.Controls.Add(this.VE2State);
            this.groupBoxValves.Controls.Add(this.label3);
            this.groupBoxValves.Controls.Add(this.VE01State);
            this.groupBoxValves.Location = new System.Drawing.Point(195, 13);
            this.groupBoxValves.Name = "groupBoxValves";
            this.groupBoxValves.Size = new System.Drawing.Size(211, 106);
            this.groupBoxValves.TabIndex = 4;
            this.groupBoxValves.TabStop = false;
            this.groupBoxValves.Text = "Состояния клапанов и насоса";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(140, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "VE11";
            // 
            // VE11State
            // 
            this.VE11State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE11State.Enabled = false;
            this.VE11State.Location = new System.Drawing.Point(179, 44);
            this.VE11State.Name = "VE11State";
            this.VE11State.Size = new System.Drawing.Size(20, 20);
            this.VE11State.TabIndex = 20;
            this.VE11State.UseVisualStyleBackColor = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(140, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "VE10";
            // 
            // VE10State
            // 
            this.VE10State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE10State.Enabled = false;
            this.VE10State.Location = new System.Drawing.Point(179, 19);
            this.VE10State.Name = "VE10State";
            this.VE10State.Size = new System.Drawing.Size(20, 20);
            this.VE10State.TabIndex = 18;
            this.VE10State.UseVisualStyleBackColor = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(75, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(27, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "VE8";
            // 
            // VE8State
            // 
            this.VE8State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE8State.Enabled = false;
            this.VE8State.Location = new System.Drawing.Point(108, 70);
            this.VE8State.Name = "VE8State";
            this.VE8State.Size = new System.Drawing.Size(20, 20);
            this.VE8State.TabIndex = 16;
            this.VE8State.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(75, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "VE6";
            // 
            // VE6State
            // 
            this.VE6State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE6State.Enabled = false;
            this.VE6State.Location = new System.Drawing.Point(108, 44);
            this.VE6State.Name = "VE6State";
            this.VE6State.Size = new System.Drawing.Size(20, 20);
            this.VE6State.TabIndex = 14;
            this.VE6State.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(151, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(22, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "NP";
            // 
            // NPState
            // 
            this.NPState.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.NPState.Enabled = false;
            this.NPState.Location = new System.Drawing.Point(179, 70);
            this.NPState.Name = "NPState";
            this.NPState.Size = new System.Drawing.Size(20, 20);
            this.NPState.TabIndex = 12;
            this.NPState.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(75, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(27, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "VE5";
            // 
            // VE5State
            // 
            this.VE5State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE5State.Enabled = false;
            this.VE5State.Location = new System.Drawing.Point(108, 18);
            this.VE5State.Name = "VE5State";
            this.VE5State.Size = new System.Drawing.Size(20, 20);
            this.VE5State.TabIndex = 8;
            this.VE5State.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "VE3";
            // 
            // VE3State
            // 
            this.VE3State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE3State.Enabled = false;
            this.VE3State.Location = new System.Drawing.Point(39, 71);
            this.VE3State.Name = "VE3State";
            this.VE3State.Size = new System.Drawing.Size(20, 20);
            this.VE3State.TabIndex = 10;
            this.VE3State.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "VE2";
            // 
            // VE2State
            // 
            this.VE2State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE2State.Enabled = false;
            this.VE2State.Location = new System.Drawing.Point(39, 44);
            this.VE2State.Name = "VE2State";
            this.VE2State.Size = new System.Drawing.Size(20, 20);
            this.VE2State.TabIndex = 8;
            this.VE2State.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "VE1";
            // 
            // VE01State
            // 
            this.VE01State.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.VE01State.Enabled = false;
            this.VE01State.Location = new System.Drawing.Point(39, 18);
            this.VE01State.Name = "VE01State";
            this.VE01State.Size = new System.Drawing.Size(20, 20);
            this.VE01State.TabIndex = 6;
            this.VE01State.UseVisualStyleBackColor = false;
            // 
            // InfoTextBox
            // 
            this.InfoTextBox.Location = new System.Drawing.Point(12, 155);
            this.InfoTextBox.Name = "InfoTextBox";
            this.InfoTextBox.ReadOnly = true;
            this.InfoTextBox.Size = new System.Drawing.Size(393, 20);
            this.InfoTextBox.TabIndex = 5;
            // 
            // BtnRun
            // 
            this.BtnRun.Location = new System.Drawing.Point(166, 124);
            this.BtnRun.Name = "BtnRun";
            this.BtnRun.Size = new System.Drawing.Size(75, 23);
            this.BtnRun.TabIndex = 6;
            this.BtnRun.Text = "Запуск";
            this.BtnRun.UseVisualStyleBackColor = true;
            this.BtnRun.Click += new System.EventHandler(this.BtnRun_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 187);
            this.Controls.Add(this.BtnRun);
            this.Controls.Add(this.InfoTextBox);
            this.Controls.Add(this.groupBoxValves);
            this.Controls.Add(this.groupBoxDetectors);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxDetectors.ResumeLayout(false);
            this.groupBoxDetectors.PerformLayout();
            this.groupBoxValves.ResumeLayout(false);
            this.groupBoxValves.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox PE1Value;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxDetectors;
        private System.Windows.Forms.TextBox BPValue;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxValves;
        private System.Windows.Forms.Button GTState;
        private System.Windows.Forms.TextBox GTValue;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button NPState;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button VE5State;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button VE3State;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button VE2State;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button VE01State;
        private System.Windows.Forms.TextBox InfoTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button VE6State;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button VE8State;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button VE11State;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button VE10State;
        private System.Windows.Forms.TextBox PE2Value;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button BtnRun;
    }
}

