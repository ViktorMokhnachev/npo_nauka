﻿using System.Collections.Generic;

namespace NPO_Nauka_Test
{
    public static class Names
    {
        public const string PE1 = "РЕ1";
        public const string PE2 = "РЕ2";
        public const string BP = "ВР";
        public const string GT = "ГТ";
        //добавлен 0, чтобы не было конфликтов с VE10 и VE11, т.к. проверка осуществляется методом Contains()
        public const string VE1 = "VE01";
        public const string VE2 = "VE2";
        public const string VE3 = "VE3";
        public const string VE5 = "VE5";
        public const string VE6 = "VE6";
        public const string VE8 = "VE8";
        public const string VE10 = "VE10";
        public const string VE11 = "VE11";
        public const string NP = "NP";
        public const string RD = "РД";
        public static List<string> detectors = new List<string>() { PE1, PE2, BP, GT};
        public static List<string> valves = new List<string>() { VE1, VE2, VE3, VE5, VE6, VE8, VE10, VE11, NP, RD };
    }
}
